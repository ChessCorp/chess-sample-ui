var chessRules = require('chess-rules');

/**
 *
 * @param pgnMoves
 */
function play(pgnMoves) {
    var position = chessRules.getInitialPosition();

    pgnMoves.forEach(function (movetext) {
        var moveCoords = chessRules.pgnToMove(position, movetext);
        position = chessRules.applyMove(position, moveCoords);
    });
    var movetext = null;

    var availableMoves = chessRules.getAvailableMoves(position);
    if (availableMoves.length > 0) {
        var selectedMove = availableMoves[Math.floor(Math.random() * availableMoves.length)];
        movetext = chessRules.moveToPgn(position, selectedMove);
    }

    return movetext;
}

module.exports.play = play;
